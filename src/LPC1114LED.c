/*
===============================================================================
 Name        : LPC1114LED.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

// TODO: insert other include files here

// TODO: insert other definitions and declarations here

int main(void) {

#if defined (__USE_LPCOPEN)
    // Read clock settings and update SystemCoreClock variable
    SystemCoreClockUpdate();
#if !defined(NO_BOARD_LIB)
    // Set up and initialize all required blocks and
    // functions related to the board hardware
    Board_Init();
    // Set the LED to the state of "On"
    Board_LED_Set(0, true);
#endif
#endif

#define TICKRATE_HZ1 (10) /* 10 ticks per second */
#define TICKRATE_HZ2 (11) /* 11 ticks per second */

    /**
     * @brief	Handle interrupt from SysTick timer
     * @return	Nothing
     */
    void SysTick_Handler(void)
    {
    	Chip_GPIO_SetPinState(LPC_GPIO, 1, 8, false);
    }

    /**
     * @brief	Handle interrupt from 32-bit timer
     * @return	Nothing
     */
    void TIMER32_0_IRQHandler(void)
    {
    	if (Chip_TIMER_MatchPending(LPC_TIMER32_0, 1)) {
    		Chip_TIMER_ClearMatch(LPC_TIMER32_0, 1);
    		Chip_GPIO_SetPinState(LPC_GPIO, 1, 8, true);
    	}
    }

    // TODO: insert code here
    //Chip_GPIO_SetPinDIR(LPC_GPIO, 1, 7, false); //Input pot
    //Chip_GPIO_SetPinDIR(LPC_GPIO, 1, 8, true); //output LED

    // Force the counter to be placed into memory
    //volatile static int i = 0 ;
    // Enter an infinite loop, just incrementing a counter

	uint32_t timerFreq;

	SystemCoreClockUpdate();
	Chip_GPIO_Init(LPC_GPIO);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, 1, 8);

	/* Enable and setup SysTick Timer at a periodic rate */
	SysTick_Config(SystemCoreClock / TICKRATE_HZ1);

	/* Enable timer 1 clock */
	Chip_TIMER_Init(LPC_TIMER32_0);

	/* Timer rate is system clock rate */
	timerFreq = Chip_Clock_GetSystemClockRate();

	/* Timer setup for match and interrupt at TICKRATE_HZ */
	Chip_TIMER_Reset(LPC_TIMER32_0);
	Chip_TIMER_MatchEnableInt(LPC_TIMER32_0, 1);
	Chip_TIMER_SetMatch(LPC_TIMER32_0, 1, (timerFreq / TICKRATE_HZ2));
	Chip_TIMER_ResetOnMatchEnable(LPC_TIMER32_0, 1);
	Chip_TIMER_Enable(LPC_TIMER32_0);

	/* Enable timer interrupt */
	NVIC_ClearPendingIRQ(TIMER_32_0_IRQn);
	NVIC_EnableIRQ(TIMER_32_0_IRQn);

    while(1) {
        //i++ ;
        //Chip_GPIO_SetPinState(LPC_GPIO, 1, 8, true);
    	//Chip_GPIO_SetPinState(LPC_GPIO, 1, 8, false);
        __WFI();
    }
    return 0 ;
}
